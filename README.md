## Need Matchmaking Backend Service

### Owner
- #team-needs-matchmaking

### Prerequisites
- [Git](https://www.atlassian.com/git/tutorials/install-git)
- [Go](https://golang.org/doc/install)

### Getting started
- Install the [prerequisites](#prerequisites)

- Clone this repository
    ```sh
    git clone git@gitlab.com:kawalcovid19/needs-matchmaking/backend.git
    ```

- Copy `.env` from `env.sample` and modify the configuration value appropriately

- Build binary
    ```sh
    make all
    ```
- Run executable file
    ```sh
    bin/server
    ```

### To Contribute
- Find Open ticket and Issues list
- create branch name with prefix or suffix as ticket name
- run make help: to get information
- create Merge Request and wait at least 1 reviewer

Thank you for contributing