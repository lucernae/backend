package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/kawalcovid19/needs-matchmaking/backend/internal/register"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/kit"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/server"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/storage/mongodb"
)

func main() {
	config, err := kit.LoadConfig()
	if err != nil {
		log.Fatalf("error initiate config : %v", err)
	}

	db, err := mongodb.NewClient(config.DB)
	if err != nil {
		log.Fatalf("error initiate database : %v", err)
	}

	beneficiaryRepos := mongodb.NewBeneficiaryRepository(db)

	regService, err := register.NewRegisterService(beneficiaryRepos)
	if err != nil {
		log.Fatalf("error initiating service : %v", err)
	}

	regHandler, err := register.NewRegisterHandler(regService)
	if err != nil {
		log.Fatalf("error initiating handler : %v", err)
	}

	startServer(config.Port, regHandler)
}

func startServer(port string, handlers ...server.Handler) {
	h := server.Build(handlers...)
	s := &http.Server{
		Addr:         fmt.Sprintf(":%s", port),
		Handler:      h,
		ReadTimeout:  300 * time.Second,
		WriteTimeout: 300 * time.Second,
	}

	sigChan := make(chan os.Signal, 1)
	signal.Notify(sigChan, os.Interrupt, syscall.SIGTERM)
	go func(s *http.Server) {
		log.Printf("matchmaking-backend service is available at %s", s.Addr)
		if serr := s.ListenAndServe(); serr != http.ErrServerClosed {
			log.Fatal(serr)
		}
	}(s)

	<-sigChan
	log.Printf("Shutting down the chat service...")

	err := s.Shutdown(context.Background())
	if err != nil {
		log.Fatalf("Something wrong when stopping server : %v", err)
		return
	}

	log.Printf("Chat service gracefully stopped")
}
