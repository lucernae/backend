module gitlab.com/kawalcovid19/needs-matchmaking/backend

go 1.12

require (
	github.com/google/uuid v1.1.1
	github.com/julienschmidt/httprouter v1.3.0
	github.com/subosito/gotenv v1.2.0
	github.com/vrischmann/envconfig v1.2.0
	go.mongodb.org/mongo-driver v1.3.1
)
