package register

import "fmt"

type BeneficiaryPayload struct {
	Name                string `json:"name"`
	Category            string `json:"category"`
	Amount              int    `json:"amount"`
	ReceiverName        string `json:"receiver_name"`
	Phone               string `json:"phone"`
	IsWhatsAppAvailable bool   `json:"is_whatsapp_available"`
	Note                string `json:"note"`
}

func (b *BeneficiaryPayload) Validate() error {
	if b.Name == "" {
		return fmt.Errorf("name is missing")
	}
	if b.Category == "" {
		return fmt.Errorf("category is missing")
	}
	if b.Amount == 0 {
		return fmt.Errorf("amount is missing")
	}
	if b.ReceiverName == "" {
		return fmt.Errorf("receiver name is missing")
	}
	if b.Phone == "" {
		return fmt.Errorf("phone is missing")
	}
	return nil
}
