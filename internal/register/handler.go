package register

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

var (
	ioutilReadAll = ioutil.ReadAll
)

type RegisterHandler struct {
	regServ RegisterService
}

func NewRegisterHandler(rserv RegisterService) (*RegisterHandler, error) {
	return &RegisterHandler{
		regServ: rserv,
	}, nil
}

func (rh *RegisterHandler) Register(router *httprouter.Router) {
	router.HandlerFunc("POST", "/beneficiary", rh.CreateBeneficiary)
}

func (rh *RegisterHandler) CreateBeneficiary(w http.ResponseWriter, r *http.Request) {
	body, err := ioutilReadAll(r.Body)
	if err != nil {
		log.Printf("Error while read body : %v", err)
		http.Error(w, "bad request body", http.StatusBadRequest)
		return
	}

	var payload BeneficiaryPayload
	if err = json.Unmarshal(body, &payload); err != nil {
		log.Printf("Error while unmarshal : %v", err)
		http.Error(w, "bad request body", http.StatusBadRequest)
		return
	}

	if err = payload.Validate(); err != nil {
		log.Printf("Error while validate payload : %v", err)
		http.Error(w, "bad request body", http.StatusBadRequest)
		return
	}

	beneficiary, err := rh.regServ.SaveBeneficiary(payload)
	if err != nil {
		log.Printf("Error while create beneficiary : %v", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	res, _ := json.Marshal(beneficiary)
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)
	_, _ = w.Write(res)
}
