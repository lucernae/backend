package register

import (
	"time"

	"github.com/google/uuid"

	"gitlab.com/kawalcovid19/needs-matchmaking/backend/internal"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/models"
)

type RegisterService interface {
	SaveBeneficiary(payload BeneficiaryPayload) (*models.Beneficiary, error)
}

type registerService struct {
	beneficiaryRepo internal.BeneficiaryRepository
}

func NewRegisterService(beneficiary internal.BeneficiaryRepository) (RegisterService, error) {
	return &registerService{
		beneficiaryRepo: beneficiary,
	}, nil
}

func (r *registerService) SaveBeneficiary(payload BeneficiaryPayload) (*models.Beneficiary, error) {
	beneficiary := new(models.Beneficiary)
	beneficiary.ID = uuid.New().String()

	beneficiary.Name = payload.Name
	beneficiary.Category = payload.Category
	beneficiary.Amount = payload.Amount
	beneficiary.ReceiverName = payload.ReceiverName
	beneficiary.Phone = payload.Phone
	beneficiary.IsWhatsAppAvailable = payload.IsWhatsAppAvailable
	beneficiary.CreatedAt = time.Now().UTC()

	beneficiary.IsActive = true

	beneficiary, err := r.beneficiaryRepo.Save(beneficiary)
	if err != nil {
		return nil, err
	}

	return beneficiary, nil
}
