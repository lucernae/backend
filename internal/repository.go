package internal

import "gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/models"

// BeneficiaryRepository interface for benificiary repo
type BeneficiaryRepository interface {
	Save(beneficiary *models.Beneficiary) (*models.Beneficiary, error)
}
