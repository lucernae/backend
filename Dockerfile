FROM busybox:1.31.1
MAINTAINER Mufti <muftiismirizqi@gmail.com>

WORKDIR /opt/needs-matchmaking

COPY bin/server ./server

RUN chmod +x ./server

CMD ["./server"]


