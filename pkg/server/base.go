package server

import (
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

// Handler is abstrac interface to build handler
type Handler interface {
	Register(*httprouter.Router)
}

// Build is function to register handler in params in route
func Build(handlers ...Handler) http.Handler {
	router := httprouter.New()

	router.HandlerFunc("GET", "/health", Health)

	for _, reg := range handlers {
		reg.Register(router)
	}

	router.NotFound = http.HandlerFunc(notFound)

	return router
}

func notFound(w http.ResponseWriter, _ *http.Request) {
	http.Error(w, "not found", http.StatusNotFound)
}

// Health is initial handler return success ok string for heartbeat check
func Health(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	fmt.Fprintf(w, "ok")
}
