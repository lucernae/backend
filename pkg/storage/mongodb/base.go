package mongodb

import (
	"context"
	"fmt"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/kit"
)

const connectStr = "mongodb://%s:%s@%s/?"

// Client struct for database variable
type Client struct {
	Database string
	Client   *mongo.Client
}

// NewClient create client for database
func NewClient(config kit.Database) (*Client, error) {
	//Create the URI we will use to connect to cosmosDB or mongodb
	connecturi := fmt.Sprintf(connectStr, config.Username, config.Password, config.Host)
	if config.SSL {
		connecturi += "ssl=true"
	} else {
		// for mongodb, will change with env value
		connecturi += "authSource=admin"
	}
	//Connect to the DB
	client, err := mongo.Connect(context.Background(), options.Client().ApplyURI(connecturi))
	if err != nil {
		return nil, err
	}

	return &Client{
		Database: config.Database,
		Client:   client,
	}, nil
}
