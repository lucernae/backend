package mongodb

import (
	"context"

	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/kawalcovid19/needs-matchmaking/backend/internal"
	"gitlab.com/kawalcovid19/needs-matchmaking/backend/pkg/models"
)

type beneficiaryRepository struct {
	database string
	client   *mongo.Client
}

func NewBeneficiaryRepository(dbClient *Client) internal.BeneficiaryRepository {
	return &beneficiaryRepository{
		database: dbClient.Database,
		client:   dbClient.Client,
	}
}

func (r *beneficiaryRepository) collection() *mongo.Collection {
	return r.client.Database(r.database).Collection("beneficiaries")
}

func (r *beneficiaryRepository) Save(beneficiary *models.Beneficiary) (*models.Beneficiary, error) {
	_, err := r.collection().InsertOne(context.Background(), beneficiary)
	if err != nil {
		return nil, err
	}

	return beneficiary, nil
}
