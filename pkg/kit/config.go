package kit

import (
	"fmt"

	"github.com/subosito/gotenv"
	"github.com/vrischmann/envconfig"
)

const (
	envNotFound = "open .env: no such file or directory"
)

// Config struct all config variable
type Config struct {
	Project string
	Stage   string
	Port    string

	DB Database
	// ADD other config here
}

// Database config variable
type Database struct {
	Database string
	Username string
	Password string
	Host     string
	SSL      bool
}

// LoadConfig defined in .env and parse to struct, ignore .env not found error
func LoadConfig(envs ...string) (*Config, error) {
	if err := gotenv.Load(); err != nil && fmt.Sprintf("%v", err) != envNotFound {
		return nil, err
	}

	for _, env := range envs {
		if err := gotenv.Load(env); err != nil {
			return nil, err
		}
	}

	conf := new(Config)
	if err := envconfig.Init(conf); err != nil {
		return nil, err
	}

	return conf, nil
}
