package models

import "time"

type Beneficiary struct {
	ID                  string    `json:"id" bson:"_id"`
	Name                string    `json:"name" bson:"name"`
	Category            string    `json:"category" bson:"category"`
	Amount              int       `json:"amount" bson:"amount"`
	ReceiverName        string    `json:"receiver_name" bson:"receiver_name"`
	Phone               string    `json:"phone" bson:"phone"`
	IsWhatsAppAvailable bool      `json:"is_whatsapp_available" bson:"is_whatsapp_available"`
	Note                string    `json:"note" bson:"note"`
	IsActive            bool      `json:"is_active" bson:"is_active"`
	CreatedAt           time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt           time.Time `json:"updated_at" bson:"updated_at"`
}
